l = map(int, "1 4 5 6 7 8 15 12 9 4 9 8 12 14 22 45 67 89 87 86 85 23 56 67 21 88 11 44 56 91 67 45 45 45 45 45 44 21 89 90 90 87 45 91 12 45 57".split())
c = 0
for i in range(1, max(l) + 1):
    for j in range(len(l) - 1):
        if l[j] >= i and l[j + 1] < i:
            c += 1
    if l[-1] >= i:
        c += 1
print c
