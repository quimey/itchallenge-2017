l = [[] for i in range(21)]
i = 0
for x in open("level4.txt"):
	if i % 22:
		l[i//22].append(x[:-1])
	i += 1

def check(a, b, c):
	if a < 0 or a > 20:
		return False
	if b < 0 or b > 20:
		return False
	if c < 0 or c > 20:
		return False
	return True

dp = {}
def calc(a, b, c, i, j, k):
	if a == i and b == j and c == k:
		return 1
	if a > i or b > j or c > k:
		return 0
	if (a, b, c, i, j, k) in dp:
		return dp[(a, b, c, i, j, k)]
	res = 0
	if check(a + 1, b, c):
		if check(i - 1, j, k):
			if l[a + 1][b][c] == l[i - 1][j][k]:
				res += calc(a + 1, b, c, i - 1, j, k)
		if check(i, j - 1, k):
			if l[a + 1][b][c] == l[i][j - 1][k]:
				res += calc(a + 1, b, c, i, j - 1, k)
		if check(i, j, k - 1):
			if l[a + 1][b][c] == l[i][j][k - 1]:
				res += calc(a + 1, b, c, i, j, k - 1)
	if check(a, b + 1, c):
		if check(i - 1, j, k):
			if l[a][b + 1][c] == l[i - 1][j][k]:
				res += calc(a, b + 1, c, i - 1, j, k)
		if check(i, j - 1, k):
			if l[a][b + 1][c] == l[i][j - 1][k]:
				res += calc(a, b + 1, c, i, j - 1, k)
		if check(i, j, k - 1):
			if l[a][b + 1][c] == l[i][j][k - 1]:
				res += calc(a, b + 1, c, i, j, k - 1)
	if check(a, b, c + 1):
		if check(i - 1, j, k):
			if l[a][b][c + 1] == l[i - 1][j][k]:
				res += calc(a, b, c + 1, i - 1, j, k)
		if check(i, j - 1, k):
			if l[a][b][c + 1] == l[i][j - 1][k]:
				res += calc(a, b, c + 1, i, j - 1, k)
		if check(i, j, k - 1):
			if l[a][b][c + 1] == l[i][j][k - 1]:
				res += calc(a, b, c + 1, i, j, k - 1)
	dp[(a, b, c, i, j, k)] = res
	return res

print calc(0, 0, 0, 20, 20, 20)
