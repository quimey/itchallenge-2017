def is_prime(x):
    for i in range(2, x):
        if x % i == 0:
            return False
    return True
ps = []
for x in range(2, 256):
    if is_prime(x):
        ps.append(x)
p2 = set([0] + [2 ** i for i in range(8)])
u = []
for l in open("level13.txt"):
    try:    
        u.append(int(l))
    except:
        pass
print len(u)
mc = 0
for p in ps:
    v = []
    for i, x in enumerate(u):
        if x ^ p in p2:
            v.append(i)
    w = set(v)
    for i in range(1, len(v)):
        for j in range(i):
            n = v[i] - v[j]
            c = 2
            while (v[j] + c * n) in w:
                c += 1
            if c > mc:
                mc = c
                mp = p
                mn = n
                print mc, mp, mn 

print mc, mp, mn 
