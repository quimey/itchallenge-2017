#include <vector>
#include <iostream>
#include <algorithm>
#include <iomanip>
#include <set>
#include <cassert>
#include <map>

using namespace std;

#define forn(i, n) for(int i = 0; i < (int) (n); i++)
#define dforn(i, n) for(int i = (int) (n) - 1; i >= 0; i--)

typedef long long int tint;
const int n = 6;
const int u = (1 << n) - 1;
int f(int i, int j) {
    return i + n * j;
}

int t[n][n];

tint bt(int a, int b, int c, int i, int j) {
    if(i == n) return bt(b, c, 0, 0, j + 1);
    if(j == n) return (a == u && b == 0)? 1: 0;
    tint res = 0;
    // la de mi izquierda esta vacia
    if((a & (1 << i)) == 0) {
        a |= (1 << i);
        // el bicho de la izquierda salto a donde estoy yo
        if(t[i][j] == f(i, j - 1)) return 0;
        t[i][j - 1] = f(i, j);
        res = bt(a, b, c, i + 1, j);
        t[i][j - 1] = -1;
        return res;
    }
    // para arriba
    if(i && (b & (1 << (i - 1))) == 0 && t[i][j] != f(i - 1, j)) {
        t[i - 1][j] = f(i, j);
        res += bt(a, b | (1 << (i - 1)), c, i + 1, j);
        t[i - 1][j] = -1;
    }
    // para abajo
    if(i + 1 < n && (b & (1 << (i + 1))) == 0) {
        t[i + 1][j] = f(i, j);
        res += bt(a, b | (1 << (i + 1)), c, i + 1, j);
        t[i + 1][j] = -1;
    }
    // para adelante
    if(j + 1 < n && (c & (1 << i)) == 0) {
        t[i][j + 1] = f(i, j);
        res += bt(a, b, c | (1 << i), i + 1, j);
        t[i][j + 1] = -1;
    }
    return res;
}

int main() {
    forn(i, n) forn(j, n) t[i][j] = -1;
    cout << bt(u, 0, 0, 0, 0) << endl;
}
