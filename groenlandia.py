import png
from collections import defaultdict
import json


dx = [1, 0, -1, 0]
dy = [0, 1, 0, -1]
pat = [1] * 7 + [0] * 6 + [3] * 7 + [2] * 4 + [1] * 5 + [0] * 3
sz = 13
lineas = []

def read(x, y):
    xx = x + 1
    yy = y + 1
    c = 0
    for p in pat:
        c *= 2
        c += 0 if lineas[xx][yy] else 1
        xx += sz * dx[p]
        yy += sz * dy[p]    
    return c

def get_program(name):
    w, h, f, md = png.Reader('server/data/{0}.png'.format(name)).read()
    for l in f:
        lineas.append(l)
    program = []
    x = 0
    y = 0
    while y < w:
        ins = {}
        x = 0
        while x < h and lineas[x][y]:
            x += 1
        if x == h:
            y += 1
            continue
        ins['code'] = read(x, y)
        x += sz * 8
        ins['args'] = []
        for i in range(4):
            while x < h and lineas[x][y]:
                x += 1
            if x >= h:
                break
            ins['args'].append(read(x, y))
            x += 100
        y += sz * 9
        program.append(ins)
    return program

from program import p
#name = raw_input()
#program = get_program(name)
program = p

mem = defaultdict(int)
def parse(arg):
    if arg >= 2 ** 31:
        return arg - 2 ** 31
    return arg - 2 ** 30

def get(arg):
    if arg >= 2 ** 31:
        return arg - 2 ** 31
    return mem[arg - 2 ** 30]
    
def pretty(ins):
    code = ins['code']
    args = ins['args']
    op = ""
    if code == 500451294:
        op = 'print'
    elif code == 1452648002:
        op = '='
    elif code == 2411814630:
        op = '+'
    elif code == 117317011:
        op = 'read'
    elif code == 155318441:
        op = '-'
    elif code == 1663342218:
        op = '^'
    elif code == 297089748:
        op = 'min'
    elif code == 2552305222:
        op = '?/*'
    elif code == 97751171:
        op = 'jmpnz'
    elif code == 2349136770:
        op = '?/jmp'
    return op + ' ' + ' - '.join(map(str, map(parse, args)))
#print program
pointer = 0
cr = 0
output = ""
while pointer < len(program):
    ins = program[pointer]
    pointer += 1
    code = ins['code']
    args = ins['args']
    #print pretty(ins), mem
    if code == 500451294:
        #print 'eaea', get(args[0])
        output += str(get(args[0]))
    elif code == 1452648002:
        mem[parse(args[0])] = get(args[1])
    elif code == 2411814630:
        mem[parse(args[0])] = get(args[1]) | get(args[2])
    elif code == 117317011:
        cr += 1
        mem[parse(args[0])] = input()
    elif code == 155318441:
        mem[parse(args[0])] = get(args[1]) - get(args[2])
    elif code == 1663342218:
        mem[parse(args[0])] = get(args[1]) ^ get(args[2])
    elif code == 297089748:
        mem[parse(args[0])] = get(args[1]) // (2 ** get(args[2])) 
    elif code == 2552305222:
        mem[parse(args[0])] = get(args[1]) & get(args[2]) # super ponele
    elif code == 97751171:
        if get(args[1]):
            pointer = parse(get(args[0]))
    elif code == 2349136770:
        if not get(args[1]):
            pointer = parse(get(args[0])) # super ponele
    else:
        print 'unknown code ' + json.dumps(ins) + ' - '.join(map(str, map(get, args)))

print mem
print cr
print output
for i in range(0, len(output), 8):
    print i, output[i:i + 8], chr(int(output[i:i + 8], 2))
        
    
