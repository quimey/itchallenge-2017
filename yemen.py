l = ['D'] * 3 + ['F'] * 3 + ['C'] * 3 + ['M']
c = 0
for x in Permutations(l):
    sirve = False
    for i in range(3):
        f = set([x[3 * i], x[3 * i + 1],  x[3 * i + 2]])
        if len(f) == 1 or (len(f) == 2 and 'M' in f):
            sirve = True
        f = set([x[i], x[i + 3],  x[i + 6]])
        if len(f) == 1 or (len(f) == 2 and 'M' in f):
            sirve = True
    if sirve:
        c += 1
print c
