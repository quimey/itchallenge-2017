t = []
for l in open('alemania.txt'):
    t.append(map(int, l.split()))
# print t
print 'read file'
m = [[(-1, 0), (0, -1)], [(-1, 0), (0, 1)], [(1, 0), (0, 1)], [(1, 0), (0, -1)]]
d = {}

def add(p, q):
    return (p[0] + q[0], p[1] + q[1])

def check(p):
    if p[0] < 0 or p[0] > 85:
        return False
    if p[1] < 0 or p[1] > 85:
        return False
    return True
    
def calc(p1, p2, p3, p4):
    if p1 == (0, 0) and p2 == (0, 85) and p3 == (85, 85) and p4 == (85, 0):
        return 1
    if (p1, p2, p3, p4) in d:
        return d[(p1, p2, p3, p4)]
    res = 0
    for q1 in m[0]:
        r1 = add(p1, q1)
        if check(r1):
            for q2 in m[1]:
                r2 = add(p2, q2)
                if check(r2):
                    for q3 in m[2]:
                        r3 = add(p3, q3)
                        if check(r3):
                            for q4 in m[3]:
                                r4 = add(p4, q4)
                                if check(r4):
                                    x = t[r1[0]][r1[1]] + t[r2[0]][r2[1]] + t[r3[0]][r3[1]] + t[r4[0]][r4[1]]
                                    if x > 0 and len(prime_factors(x)) == 2 and is_squarefree(x):
                                        res += calc(r1, r2, r3, r4)    
                           
    d[(p1, p2, p3, p4)] = res
    return res
    
print calc((42, 42), (42, 43), (43, 43), (43, 42))
