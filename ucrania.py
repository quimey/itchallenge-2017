import base64
s=["vcve2Yqvo67e3ebi0u3g2PGgpMPju9few6k=",
    "wtLZ4Yqgor3i1tzu5JGzuOapuuP1bg==",
    "zM/f1oqwsqvh3eXc5fDwkLLQ8a3d0ed8",
    "uMvR3Zmot6zcz+Ls+ZnI67mVu8jzgA==",
    "u8zWz4iwp8DS19HU0/Da8JXspbvMpd/Skg==",
    "t9rk3IafsrPf4uHs3eDn6qa049O7/OH9fg==",
    "zM7j2Zelo7jy0uma05yoxMyrxWw=",
    "w8vc3oqgpavh2uX20uqV59/Qwc6r8mw=",
    "utjl15Koo73h3fbZ8pmoxc+X7ZnmVg==",
    "ydrf2oedsKzc7+jw4Y7yyr3c3+z1ag==",
    "ycflzYqsn7jc3eLT3+HU75GpwO7XsfX2iA==",
    "udLR35ils73O5ODu8tvv8eWg0sPFrcf2AHw=",
    "uM/k3oquoavP3vPskeKx6s6v+L92",
    "uNXdzIulsrjP6+qNmNu10NP7t3k=",
    "yc7R15erobXo1tfh35zBrfO6xrSucw==",
    "udXdzIawp8DQ6tjR1vCX+MrH0cGxuYI=",
    "xtXnzoqusLLx7Onckc/A6sG47vZy",
    "wMviz5Kln7Lfz+jV7JbnwsuWt5urfQ==",
    "yMvd2ZmhsbLj3N7m5eWh9cfBq8bfx6g=",
    "ys7i2Zyen7PokbPqxsih4/Bw"]
    
t = []
for x in s:
    t.append(map(ord,base64.b64decode(x)))
    
p = map(ord, "meltedgalloway:wcS@D6d6")

def get_key(c, p, l):
    k = []
    for i in range(l):
        k.append((1024 + c[i] - p[i] - i) % 256)
    return k

def decode(c, kk):
    p = []
    k = list(kk)
    for i in range(len(c)):
        p.append((256 + c[i] - k[i] - i) % 256)
        k.append(p[i])
    return p

for x in t:
    k = get_key(x, p, 8)
    print ''.join(map(chr, k)), ':', ''.join(map(chr, decode(x, k)))
    print "-----"
        
