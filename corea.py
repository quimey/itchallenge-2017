INF = 10 ** 40
c, k = map(int, raw_input().split())
s = []
for i in range(c):
    s.append(input())

#parado en i con j puertas por poner
dp = {}
def calc(i, j, t):
    if j == 0:
        return INF
    res = INF
    if (i, j) in dp:
        return dp[(i, j)]
    p = 0
    for a in range(i + 1, c):
        res = min(res, calc(a, j - 1, t) + p)
        p += (a - i) * t[a]
    res = min(res, p)
    dp[(i, j)] = res
    return res

ans = INF
for i in range(c):
    if i % 10 == 0:
        print "vamos ", i
    t = s[i:] + s[:i]
    dp = {}
    u = calc(0, k, t)
    if u < ans:
        print i, u
        ans = u
print ans
